# Contract administrator

## Zrealizowane zadania
Aplikacja:
- buduje się do paczki WAR
- baza danych zawiera dwie tabele:
    - Dla systemów
    - Dla kontraktów
- Tabele połączone są relacją Jeden system do Wielu kontraktów
- Dla kontraktów pełny CRUD (Delete w postaci deaktywacji)
- Pełna walidacja danych po stronie Front-end'u
- Aplikacja udostępnia REST API dla systemów oraz kontraktów
- Pełna walidacja danych po stronie serwera
- Front-end komunikuje się z użyciem w techniki AJAX, która wywołuje REST API aplikacji
- Front-end napisany z użyciem bibliotek JQuery, JQuery UI, JQuery Tables
- Część szablonowa zrealizowana z użyciem Apache Tiles
- Konfiguracja servletu dyspozytora zrealizowana z użyciem klas.
    - Wystarczy rozszrzerzyć klasę  AbstractAnnotationConfigDispatcherServletInitializer oraz przesłonić wymagane metody.
    - Do pliku pom.xml dodać konfiguracje wtyczki maven-war-plugin dodając odpowiednią pozycje, tak aby plik web.xml nie był poszukiwany.
- Implementacja funkcjonalności dodawania oraz edycji pozycji systemów
- Implementacja pliku importu plików Excel z użyciem biblioteki Apache POI
- Mechanizm cache zrealizowany z użyciem Redis
    - Niestety nie udało mi sie zrealizować pobierania TTL z pliku konfiguracyjnego 
- Mechanizm dostępu do danych zrealizowany z użyceim frameworku Hibernate oraz repozytoriów automatycznych Spring
- Dodałem kilka usprawnień które upraszczają użycie aplikacji

## Opis konfiguracji

1. Utworzyć bazę danych postgresql oraz uruchomić ze wskazaniem utworzenej bazy plik databaseInitialScript.sql
2. W klasie pl.bb166.contracts.administrator.configuration.PersistenceConfiguration należy ustawić parametry połączenia: Adres JDBC bazy danych, nazwę użytkownika, hasło dostępu.

## Opis przebiegu budowy projektu przy pomocy konsoli Maven
1. Otworzyć terminal
2. Przejść do lokalizacji pliku pom.xml projektu
3. Wywołać polecenie:
    ```
    mvn package
    ```
4. Jeśli Maven jest poprawnie skonfigurowany (tj. Ustawione są wszystkie niezbędne zmienne środowiskowe) w katalogu target powinien się pojawić zbudowany plik *.war, który można umieścić w kontenerze serwletów lub serwerze aplikacji.

## Przykładowy opis uruchomienia aplikacji z użyciem kontenera servletów Tomcat

1. Plik *.war zbudowany w poprzedmin punkcie, należy umieścić w lokalizacji %folderZProgramemTomcat%/webapps
2. Następnie w zależności od systemu, z katalogu bin Tomcat'a uruchomić odpowiedni skrypt "startup"
3. Jeśli Tomcat jest poprawnie skonfigurowany, aplikacja powinna być dostępna w lokalizacji serwera pod adresem:
    ```
    localhost:8080/[nazwaPlikuWar]/
    ```
    O ile Tomcat jest domyślnie skonfigurowany

## Krótki opis REST API 
Opis bez struktury plików JSON :( ale można sobie oczywiście przeanalizować pakiet dto i będzie wszystko wiadomo :)

- końcówka /contract
    - Metoda GET - Pobranie wszystkich dostępnych kontraktów
    - Metoda DELETE, Parametr id - Deaktywacja kontraktu o danym numerze kontraktu 
    - Metoda PUT - Edycja kontraktu
    - Metoda POST - Utworzenie kontraktu
- końcówka /contract/active
    - Metoda GET - Pobranie wszystkich aktywnych 
- końcówka /system/fetchNames
    - Metoda GET - Pobranie wszystkich dostępnych nazw systemów
- końcówka /system
    - Metoda GET - Pobranie wszystkich dostępnych systemów
    - Metoda PUT - Edycja systemu
    - Metoda POST - Dodanie systemu

## Opis struktury bazy danych

- Klucze główne tabel mają założony 'auto-increment' poprzez sekwencje.
- Dla kolumny "contract_number" tabeli "contracts" założono ograniczenie unikatowości wraz z indeksem ponieważ tabela jest przeszukiwana właśnie po tej właściwości.
- Analogicznie jak wyżej dla tabeli systems dla właściwości "system_name".
- Dla wartości "amount_type" oraz "amount_period" wprowadzono odpowiednie słowniki, jednak nie są one zmapowane w aplikacji aby zaoszczędzić niepotrzebnych złączeń. Usprawnienie zostało wprowadzone ze względu małą lczbę rekordów w słowniku.
