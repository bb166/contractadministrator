import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import pl.bb166.contracts.administrator.domain.dto.SystemEditRequest;
import pl.bb166.contracts.administrator.domain.entity.SystemEntity;
import pl.bb166.contracts.administrator.exception.ValidationException;
import pl.bb166.contracts.administrator.repository.SystemRepository;
import pl.bb166.contracts.administrator.service.StandardSystemService;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class StandardSystemServiceTest {

    static StandardSystemService systemService = new StandardSystemService();

    static List<SystemEntity> mockEntities;

    static SystemRepository systemRepository = mock(SystemRepository.class);

    @BeforeClass
    public static void init() {
        systemService.setSystemRepository(systemRepository);

        SystemEntity entity1 = new SystemEntity();
        entity1.setName("System1");
        entity1.setDescription("Desc1");
        entity1.setTechnologyDescription("TechDesc1");

        SystemEntity entity2 = new SystemEntity();
        entity2.setName("System2");
        entity2.setDescription("Desc2");
        entity2.setTechnologyDescription("TechDesc2");

        SystemEntity entity3 = new SystemEntity();
        entity3.setName("System3");
        entity3.setDescription("Desc3");
        entity3.setTechnologyDescription("TechDesc3");

        mockEntities = Arrays.asList(entity1, entity2, entity3);
    }

    @Test
    public void fetchSystemNamesTest() {
        when(systemRepository.findAll()).thenReturn(mockEntities);

        List<String> list = systemService.getAllSystemNames();

        Assert.assertTrue(list.containsAll(Arrays.asList("System1", "System2")));
    }

    @Test
    public void insertSystemTest() throws ValidationException {
        SystemEntity systemEntity = mockEntities.get(0);
        when(systemRepository.findSystemEntityByName(systemEntity.getName()))
                .thenReturn(null);

        systemService.insertSystem(systemEntity);

        verify(systemRepository).save(systemEntity);
    }

    @Test(expected = ValidationException.class)
    public void insertSystemWhenSystemNameIsAvailableInDatabaseTest() throws ValidationException {
        SystemEntity systemEntity = mockEntities.get(0);
        when(systemRepository.findSystemEntityByName(systemEntity.getName()))
                .thenReturn(systemEntity);

        systemService.insertSystem(systemEntity);
    }

    @Test
    public void editSystemWhenUserHasNotEditedSystemNameTest() throws ValidationException {
        SystemEditRequest systemEdit = new SystemEditRequest();
        systemEdit.setName("System1");
        systemEdit.setOldSystemName("System1");
        systemEdit.setDescription("EditedDesc");
        systemEdit.setTechnologyDescription("EditedTechDesc");

        SystemEntity systemEntity = mock(SystemEntity.class);
        when(systemRepository.findSystemEntityByName(systemEdit.getName()))
                .thenReturn(systemEntity);

        systemService.editSystem(systemEdit);

        verify(systemEntity).setName(systemEdit.getName());
        verify(systemEntity).setDescription(systemEdit.getDescription());
        verify(systemEntity).setTechnologyDescription(systemEdit.getTechnologyDescription());
    }

    @Test
    public void editSystemWhenUserHasEditedSystemNameTest() throws ValidationException {
        SystemEditRequest systemEdit = new SystemEditRequest();
        systemEdit.setName("System123");
        systemEdit.setOldSystemName("System1");
        systemEdit.setDescription("EditedDescription");
        systemEdit.setTechnologyDescription("EditedTechnologyDescrition");

        SystemEntity systemEntity = mock(SystemEntity.class);
        when(systemRepository.findSystemEntityByName(systemEdit.getOldSystemName()))
                .thenReturn(systemEntity);

        systemService.editSystem(systemEdit);

        verify(systemEntity).setName(systemEdit.getName());
        verify(systemEntity).setDescription(systemEdit.getDescription());
        verify(systemEntity).setTechnologyDescription(systemEdit.getTechnologyDescription());
    }
}