import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import pl.bb166.contracts.administrator.domain.AmountPeriod;
import pl.bb166.contracts.administrator.domain.AmountType;
import pl.bb166.contracts.administrator.domain.dto.ContractRequest;
import pl.bb166.contracts.administrator.domain.dto.ContractRow;
import pl.bb166.contracts.administrator.domain.entity.ContractEntity;
import pl.bb166.contracts.administrator.domain.entity.SystemEntity;
import pl.bb166.contracts.administrator.exception.ValidationException;
import pl.bb166.contracts.administrator.repository.ContractRepository;
import pl.bb166.contracts.administrator.repository.SystemRepository;
import pl.bb166.contracts.administrator.service.StandardContractService;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class StandardContractServiceTest {
    static StandardContractService standardContractService = new StandardContractService();

    static ContractRepository contractRepository = mock(ContractRepository.class);

    static SystemRepository systemRepository = mock(SystemRepository.class);

    static List<ContractEntity> mockEntities;

    static SystemEntity mockSystemEntity;

    static ContractRequest mockContractRequest;

    @BeforeClass
    public static void init() {
        standardContractService.setDateFormal(ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd")));
        standardContractService.setContractRepository(contractRepository);
        standardContractService.setSystemRepository(systemRepository);

        mockSystemEntity = new SystemEntity();
        mockSystemEntity.setName("System1");

        ContractEntity mockEntity1 = new ContractEntity();
        mockEntity1.setActive(true);
        mockEntity1.setAmount(BigDecimal.valueOf(11111.11));
        mockEntity1.setAmountPeriod(AmountPeriod.MONTHLY);
        mockEntity1.setAmountType(AmountType.GROSS);
        mockEntity1.setContractNumber("111/2017");
        mockEntity1.setStartDate(new Date());
        mockEntity1.setEndDate(new Date());
        mockEntity1.setSystemEntity(mockSystemEntity);

        ContractEntity mockEntity2 = new ContractEntity();
        mockEntity2.setActive(false);
        mockEntity2.setAmount(BigDecimal.valueOf(13311.21));
        mockEntity2.setAmountPeriod(AmountPeriod.YEARLY);
        mockEntity2.setAmountType(AmountType.NET);
        mockEntity2.setContractNumber("121/2017");
        mockEntity2.setStartDate(new Date());
        mockEntity2.setEndDate(new Date());
        mockEntity2.setSystemEntity(mockSystemEntity);

        mockContractRequest = new ContractRequest();
        mockContractRequest.setAmount(mockEntity1.getAmount());
        mockContractRequest.setAmountPeriod(mockEntity1.getAmountPeriod());
        mockContractRequest.setAmountType(mockEntity1.getAmountType());
        mockContractRequest.setContractNumber(mockEntity1.getContractNumber());
        mockContractRequest.setEndDate("2015-01-01");
        mockContractRequest.setStartDate("2014-11-11");
        mockContractRequest.setSystemName("System1");

        mockSystemEntity.setContractEntities(new HashSet<>(Arrays.asList(mockEntity1, mockEntity2)));

        mockEntities = Arrays.asList(mockEntity1, mockEntity2);
    }

    @Test(expected = ValidationException.class)
    public void deactivateContractWhenContractAvailableTest() throws ValidationException {
        String id = "121/2017";

        when(contractRepository.findContractEntityByContractNumberAndActiveTrue(id)).thenReturn(null);

        standardContractService.deactivateContract(id);
    }

    @Test
    public void deactivateContractTest() throws ValidationException {
        String id = "121/2017";
        ContractEntity mock = mock(ContractEntity.class);

        when(contractRepository.findContractEntityByContractNumberAndActiveTrue(id)).thenReturn(mock);

        standardContractService.deactivateContract(id);

        verify(mock).setActive(false);
    }
}