package pl.bb166.contracts.administrator.domain.dto;

/**
 * Class which represent excel import statistics
 *
 * @author Mateusz Boral
 * @version 0.9.0
 */
public class Statistics {
    private int successfulAdded;
    private int contractNumberAvailableAlready;
    private int systemNotAvailable;
    private int wrongAmount;
    private int wrongStartDateFormat;
    private int wrongEndDateFormat;
    private int wrongOrderNumber;
    private int wrongAmountType;
    private int wrongAmountPeriod;
    private int wrongActive;

    public void incrementSuccessfulAdded () {
        successfulAdded++;
    }

    public void incrementContractNumber() {
        contractNumberAvailableAlready++;
    }

    public void incrementSystemNotAvailable() {
        systemNotAvailable++;
    }

    public void incrementWrongAmount() {
        wrongAmount++;
    }

    public void incrementWrongStartDateFormat() {
        wrongStartDateFormat++;
    }

    public void incrementWrongEndDateFormat() {
        wrongEndDateFormat++;
    }

    public void incrementWrongOrderNumber() {
        wrongOrderNumber++;
    }

    public void incrementWrongAmountType() {
        wrongAmountType++;
    }

    public void incrementWrongAmountPeriod() {
        wrongAmountPeriod++;
    }

    public void incrementWrongActive() {
        wrongActive++;
    }

    public int getSuccessfulAdded() {
        return successfulAdded;
    }

    public int getContractNumberAvailableAlready() {
        return contractNumberAvailableAlready;
    }

    public int getSystemNotAvailable() {
        return systemNotAvailable;
    }

    public int getWrongAmount() {
        return wrongAmount;
    }

    public int getWrongStartDateFormat() {
        return wrongStartDateFormat;
    }

    public int getWrongEndDateFormat() {
        return wrongEndDateFormat;
    }

    public int getWrongOrderNumber() {
        return wrongOrderNumber;
    }

    public int getWrongAmountType() {
        return wrongAmountType;
    }

    public int getWrongAmountPeriod() {
        return wrongAmountPeriod;
    }

    public int getWrongActive() {
        return wrongActive;
    }
}