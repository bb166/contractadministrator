package pl.bb166.contracts.administrator.domain.dto;

import pl.bb166.contracts.administrator.domain.AmountPeriod;
import pl.bb166.contracts.administrator.domain.AmountType;

import java.math.BigDecimal;

/**
 * Class represent data for Contract request which coming from client something update or insert
 *
 * @author Mateusz Boral
 * @version 0.9.0
 */
public class ContractRequest {
    private String contractNumber;

    private String startDate;

    private String endDate;

    private BigDecimal amount;

    private AmountType amountType;

    private AmountPeriod amountPeriod;

    private String systemName;

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public AmountType getAmountType() {
        return amountType;
    }

    public void setAmountType(AmountType amountType) {
        this.amountType = amountType;
    }

    public AmountPeriod getAmountPeriod() {
        return amountPeriod;
    }

    public void setAmountPeriod(AmountPeriod amountPeriod) {
        this.amountPeriod = amountPeriod;
    }
}