package pl.bb166.contracts.administrator.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.*;
import java.util.Set;

/**
 * Class which represent System row in database
 *
 * @author Mateusz Boral
 * @version 0.9.0
 */
@Entity
@Table(name="systems", indexes = {
        @Index(columnList= "name", name = "system_name_index")
})
@RedisHash("System")
public class SystemEntity {
    @Id
    @SequenceGenerator(name = "systemSequence", sequenceName = "systems_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "systemSequence")
    @Column(name = "system_id", unique = true, nullable = false)
    private Long id;

    @Column(unique = true)
    private String name;

    private String description;

    @Column(name = "technology_description")
    private String technologyDescription;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "systemEntity", cascade = CascadeType.PERSIST)
    private Set<ContractEntity> contractEntities;

    public Long getId() {
        return id;
    }

    private void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTechnologyDescription() {
        return technologyDescription;
    }

    public void setTechnologyDescription(String technologyDescription) {
        this.technologyDescription = technologyDescription;
    }

    public Set<ContractEntity> getContractEntities() {
        return contractEntities;
    }

    public void setContractEntities(Set<ContractEntity> contractEntities) {
        this.contractEntities = contractEntities;
    }
}
