package pl.bb166.contracts.administrator.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

/**
 *  Enum represents types of amount
 *
 * @author Mateusz Boral
 * @version 0.9.0
 */
public enum AmountType {
    NET("Netto", "NET"), GROSS("Brutto", "BRU");

    private static final Map<String, AmountType> AMOUNT_TYPE_VALUES_FOR_NAME = new HashMap<>();
    private static final Map<String, AmountType> AMOUNT_TYPE_VALUES_FOR_IMPORT = new HashMap<>();

    static {
        AMOUNT_TYPE_VALUES_FOR_NAME.put(NET.getName(), NET);
        AMOUNT_TYPE_VALUES_FOR_NAME.put(GROSS.getName(), GROSS);
        AMOUNT_TYPE_VALUES_FOR_IMPORT.put(NET.getNameForImport(), NET);
        AMOUNT_TYPE_VALUES_FOR_IMPORT.put(GROSS.getNameForImport(), GROSS);
    }

    private String name;
    private String nameForImport;

    @JsonCreator
    public static AmountType forName(String name) {
        return AMOUNT_TYPE_VALUES_FOR_NAME.get(name);
    }

    public static AmountType forImport(String name) {
        return AMOUNT_TYPE_VALUES_FOR_IMPORT.get(name);
    }

   AmountType(String name, String nameForImport) {
        this.name = name;
        this.nameForImport = nameForImport;
    }

    public String getNameForImport() {
        return nameForImport;
    }

    @JsonValue
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}


