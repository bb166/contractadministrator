package pl.bb166.contracts.administrator.domain.dto;

/**
 * Class used to response special situation when application threw exception
 * @author Mateusz Boral
 * @version 0.9.0
 */
public class ExceptionResponse {
    private String message;
    private String code;

    public ExceptionResponse(String message, String code) {
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
