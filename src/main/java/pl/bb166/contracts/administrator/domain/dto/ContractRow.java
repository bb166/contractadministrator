package pl.bb166.contracts.administrator.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import pl.bb166.contracts.administrator.domain.AmountPeriod;
import pl.bb166.contracts.administrator.domain.AmountType;
import pl.bb166.contracts.administrator.domain.entity.ContractEntity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Class represent row in database which can send to client
 * @author Mateusz Boral
 * @version 0.9.0
 */
public class ContractRow {

    private Long id;

    private String contractNumber;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date startDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date endDate;

    private BigDecimal amount;

    private String systemName;

    private AmountPeriod amountPeriod;

    private AmountType amountType;

    private Boolean active;

    public ContractRow() {}

    public ContractRow(ContractEntity contractEntityEntity) {
        this.id = contractEntityEntity.getId();
        this.contractNumber = contractEntityEntity.getContractNumber();
        this.startDate = contractEntityEntity.getStartDate();
        this.endDate = contractEntityEntity.getEndDate();
        this.amount = contractEntityEntity.getAmount();
        this.active = contractEntityEntity.getActive();
        this.systemName = contractEntityEntity.getSystemEntity().getName();
        this.amountPeriod = contractEntityEntity.getAmountPeriod();
        this.amountType = contractEntityEntity.getAmountType();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public AmountPeriod getAmountPeriod() {
        return amountPeriod;
    }

    public void setAmountPeriod(AmountPeriod amountPeriod) {
        this.amountPeriod = amountPeriod;
    }

    public AmountType getAmountType() {
        return amountType;
    }

    public void setAmountType(AmountType amountType) {
        this.amountType = amountType;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}