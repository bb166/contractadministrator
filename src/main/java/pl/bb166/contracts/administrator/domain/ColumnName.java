package pl.bb166.contracts.administrator.domain;

import java.util.EnumSet;
import java.util.Optional;
import java.util.Set;

/**
 * Enumeration which contains column names to create head structure of file to export
 *
 * @author Mateusz Boral
 * @version 0.9.0
 * @see pl.bb166.contracts.administrator.service.ExportContractService
 * @see pl.bb166.contracts.administrator.service.StandardExportContractService
 */
public enum ColumnName {
    SYSTEM_NAME("system"),
    ORDER_NUMBER("order_number"),
    FROM_DATE("from_date"),
    TO_DATE("to_date"),
    AMOUNT("amount"),
    AMOUNT_TYPE("amount_type"),
    AMOUNT_PERIOD("amount_period"),
    ACTIVE("active");

    public static final Set<ColumnName> COLUMN_NAMES = EnumSet.allOf(ColumnName.class);

    /**
     * Get instance from name which is first parameter of instance
     * @param name Name to find
     * @return Optional of instance
     */
    public static Optional<ColumnName> getForName(String name) {
        for (ColumnName columnName : COLUMN_NAMES)
            if (columnName.getNativeName().equals(name))
                return Optional.of(columnName);
        return Optional.empty();
    }

    private String nativeName;

    ColumnName(String nativeName) {
        this.nativeName = nativeName;
    }

    public String getNativeName() {
        return nativeName;
    }
}
