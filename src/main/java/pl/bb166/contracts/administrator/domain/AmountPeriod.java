package pl.bb166.contracts.administrator.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

/**
 *  Enum represents type of amount period
 *
 * @author Mateusz Boral
 * @version 0.9.0
 */
public enum AmountPeriod {
    MONTHLY("Miesiecznie", "MONTH"), YEARLY("Rocznie", "YEAR");

    private static final Map<String, AmountPeriod> AMOUNT_PERIOD_VALUES_FOR_VIEW = new HashMap<>();
    private static final Map<String, AmountPeriod> AMOUNT_PERIOD_VALUES_FOR_IMPORT = new HashMap<>();

    static {
        AMOUNT_PERIOD_VALUES_FOR_VIEW.put(MONTHLY.getNameForView(), MONTHLY);
        AMOUNT_PERIOD_VALUES_FOR_VIEW.put(YEARLY.getNameForView(), YEARLY);
        AMOUNT_PERIOD_VALUES_FOR_IMPORT.put(MONTHLY.nameForImport, MONTHLY);
        AMOUNT_PERIOD_VALUES_FOR_IMPORT.put(YEARLY.nameForImport, YEARLY);
    }

    @JsonCreator
    public static AmountPeriod getByNameForView(String name) {
        return AMOUNT_PERIOD_VALUES_FOR_VIEW.get(name);
    }

    public static AmountPeriod getByNameForImport(String name) {
        return AMOUNT_PERIOD_VALUES_FOR_IMPORT.get(name);
    }

    private String nameForView;
    private String nameForImport;

    AmountPeriod(String nameForView, String nameForImport) {
        this.nameForView = nameForView;
        this.nameForImport = nameForImport;
    }

    @JsonValue
    public String getNameForView() {
        return nameForView;
    }

    public void setNameForView(String nameForView) {
        this.nameForView = nameForView;
    }

}