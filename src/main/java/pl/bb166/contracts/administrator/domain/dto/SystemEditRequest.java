package pl.bb166.contracts.administrator.domain.dto;

/**
 * Class used to update systems in database.
 * Class include new property named oldSystemName used to distinguish system in database when systemName property was edit
 *
 * @author Mateusz Boral
 * @version 0.9.0
 */
public class SystemEditRequest {
    private String name;

    private String description;

    private String technologyDescription;

    private String oldSystemName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTechnologyDescription() {
        return technologyDescription;
    }

    public void setTechnologyDescription(String technologyDescription) {
        this.technologyDescription = technologyDescription;
    }

    public String getOldSystemName() {
        return oldSystemName;
    }

    public void setOldSystemName(String oldSystemName) {
        this.oldSystemName = oldSystemName;
    }
}
