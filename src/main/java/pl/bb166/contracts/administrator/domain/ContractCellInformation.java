package pl.bb166.contracts.administrator.domain;

/**
 * Class contains information about single head cell of export file
 *
 * @author Mateusz Boral
 * @version 0.9.0
 */
public class ContractCellInformation {
    /**
     * Absolute number of cell
     */
    private int absoluteNumber;
    /**
     * Instance of column name which represent column type
     */
    private ColumnName columnName;

    public ContractCellInformation(int absoluteNumber, ColumnName columnName) {
        this.absoluteNumber = absoluteNumber;
        this.columnName = columnName;
    }

    public int getAbsoluteNumber() {
        return absoluteNumber;
    }

    public ColumnName getColumnName() {
        return columnName;
    }
}
