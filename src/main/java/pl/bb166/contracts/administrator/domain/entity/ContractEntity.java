package pl.bb166.contracts.administrator.domain.entity;

import org.springframework.data.redis.core.RedisHash;
import pl.bb166.contracts.administrator.domain.AmountPeriod;
import pl.bb166.contracts.administrator.domain.AmountType;
import pl.bb166.contracts.administrator.domain.dto.ContractRow;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Class which represent Contract row in database
 *
 * @author Mateusz Boral
 * @version 0.9.0
 */

@Entity
@Table(name = "contracts", indexes = {
        @Index(columnList = "contract_number", name = "contract_number_index")
})
@RedisHash("Contract")
public class ContractEntity {
    @Id
    @SequenceGenerator(name = "contractSequence", sequenceName = "contracts_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contractSequence")
    @Column(name = "contract_id", unique = true, nullable = false)
    private Long id;

    @Column(name = "contract_number", unique = true)
    private String contractNumber;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "amount")
    private BigDecimal amount;

    @Enumerated
    @Column(name = "amount_period")
    private AmountPeriod amountPeriod;

    @Enumerated
    @Column(name = "amount_type")
    private AmountType amountType;

    private Boolean active;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "system_id")
    private SystemEntity systemEntity;

    public ContractEntity() {}

    public ContractEntity(ContractRow row) {
        this.active         = row.getActive();
        this.amount         = row.getAmount();
        this.amountPeriod   = row.getAmountPeriod();
        this.amountType     = row.getAmountType();
        this.contractNumber = row.getContractNumber();
        this.endDate        = row.getEndDate();
        this.startDate      = row.getStartDate();
    }

    public Long getId() {
        return id;
    }

    private void setId(Long id) {
        this.id = id;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public AmountPeriod getAmountPeriod() {
        return amountPeriod;
    }

    public void setAmountPeriod(AmountPeriod amountPeriod) {
        this.amountPeriod = amountPeriod;
    }

    public AmountType getAmountType() {
        return amountType;
    }

    public void setAmountType(AmountType amountType) {
        this.amountType = amountType;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public SystemEntity getSystemEntity() {
        return systemEntity;
    }

    public void setSystemEntity(SystemEntity systemEntity) {
        this.systemEntity = systemEntity;
    }

    public void updateFormEntity(ContractEntity entity) {
        this.amount 		= entity.amount;
        this.endDate 		= entity.endDate;
        this.startDate 		= entity.startDate;
        this.systemEntity = entity.systemEntity;
        this.amountPeriod	= entity.amountPeriod;
        this.amountType		= entity.amountType;
    }
}
