package pl.bb166.contracts.administrator.controller;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.bb166.contracts.administrator.domain.dto.ExceptionResponse;
import pl.bb166.contracts.administrator.exception.ValidationException;

import java.text.ParseException;

/**
 * Global exception handle controller which catch all exceptions and convert them to http response
 * @author Mateusz Boral
 * @version 0.9.0
 */
@ControllerAdvice
public class GlobalExceptionHandleController {
    final Logger logger = Logger.getLogger(getClass());

    /**
     *  Handling exception method for ParseException
     *
     * @param ex caught exception
     * @return Exception http response
     */
    @ExceptionHandler(ParseException.class)
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public @ResponseBody ExceptionResponse parseHandler(ParseException ex) {
        return new ExceptionResponse(ex.getMessage(), "5");
    }

    /**
     *  Handling exception method for ValidationException
     *
     * @param ex caught exception
     * @return Exception http response
     */
    @ExceptionHandler(ValidationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    private @ResponseBody
    ExceptionResponse validationHandler(ValidationException ex) {
        logger.debug("Thrown ValidationException: " + ex.getMessage());
        return new ExceptionResponse(ex.getMessage(), ex.getCode());
    }
}
