package pl.bb166.contracts.administrator.controller;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.bb166.contracts.administrator.domain.dto.ContractRequest;
import pl.bb166.contracts.administrator.domain.dto.ContractRow;
import pl.bb166.contracts.administrator.domain.dto.Statistics;
import pl.bb166.contracts.administrator.exception.ValidationException;
import pl.bb166.contracts.administrator.service.ContractService;
import pl.bb166.contracts.administrator.service.ExportContractService;

import java.io.InputStream;
import java.text.ParseException;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Defines all operation relates from Contract entities control and do validation for Requests.
 * Controller have upload excel file functionality to export.
 * Controller depends ContractService and ExportContractService implementations.
 *
 * @author Mateusz Boral
 * @version 0.9.0
 */
@RestController
@RequestMapping("/contract")
public class ContractController {
    private final Logger logger = Logger.getLogger(getClass());

    /**
     * Pattern objects to fields validation on requests
     */
    private Pattern date  = Pattern.compile("\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12][0-9]|3[01])*");
    private Pattern contractNumber = Pattern.compile("^\\d+(\\/\\d{4})$");

    private ContractService contractService;

    private ExportContractService exportContractService;

    @Autowired
    public void setExportContractService(ExportContractService exportContractService) {
        this.exportContractService = exportContractService;
    }

    @Autowired
    public void setContractService(ContractService contractService) {
        this.contractService = contractService;
    }

    /**
     * Method witch handling Get http requests return all active contracts from database
     * @return List of ContractRow
     */
    @GetMapping(value = "active")
    public List<ContractRow> getActiveContractTable() {
        return contractService.getAllActiveContracts();
    }

    /**
     *  Method witch handling Get http requests return all contracts from database
     * @return List of ContractRow
     */
    @GetMapping
    public List<ContractRow> getAllContractTable() {
        logger.debug("Received get mapping for /contract endpoint");
        return contractService.getAllContracts();
    }

    /**
     * Method handling insert contract request
     *
     * @param createContractRequest Request from caller
     * @throws ValidationException when request have wrong content
     * @throws ParseException never thrown because date will be validate before parse
     */
    @PostMapping(consumes = "application/json")
    public void insertContract(@RequestBody ContractRequest createContractRequest) throws ValidationException, ParseException {
        validationContractRequest(createContractRequest);
        contractService.createContractFromRequest(createContractRequest);
    }

    /**
     * Method witch handling contract update requests
     * @param createContractRequest Request form caller
     * @throws ValidationException when request have wrong content
     * @throws ParseException never thrown because date will be validate before parse
     */
    @PutMapping(consumes = "application/json")
    public void updateContract(@RequestBody ContractRequest createContractRequest) throws ValidationException, ParseException {
        validationContractRequest(createContractRequest);
        contractService.updateContractFromRequest(createContractRequest);
    }

    /**
     * Method with handling deactivation requests
     * @param id Contract number to handle entity in database
     * @throws ValidationException when entity is not available in database
     */
    @DeleteMapping
    public void deactivateContract(@RequestParam String id) throws ValidationException {
        contractService.deactivateContract(id);
    }

    /**
     * Method to handling upload Excel file to export to database
     *
     * @param file from upload
     * @return Statistics of processed by service
     * @throws Exception
     */
    @PostMapping(value = "importFile")
    public @ResponseBody Statistics exportFile(@RequestBody MultipartFile file) throws Exception {
        logger.debug("Received multipart file: " + file.getName());

        InputStream is = file.getInputStream(); // Get input stream from uploaded file

        Workbook workbook;

        try {
            workbook = WorkbookFactory.create(is);  // Call factory method to create form InputStream
                                                    // Factory method guarantee implementation independent from file version
        } catch (Exception ex) {
            throw new ValidationException("99","Something gone wrong"); // Catch Unchecked exception...
                                                                        // I know that is wrong practise but I haven't found solution
                                                                        // how check file extension
        }

        return exportContractService.exportToRepository(workbook); // Try exports file to database by service
    }

    private void validationContractRequest(ContractRequest contractRequest) throws ValidationException{
        if (isNullOrEmpty(contractRequest.getContractNumber()) || !contractNumber.matcher(contractRequest.getContractNumber()).matches())
            throw new ValidationException("100", "Contract number is empty or have wrong pattern");
        if (isNullOrEmpty(contractRequest.getEndDate()) || !date.matcher(contractRequest.getEndDate()).matches())
            throw new ValidationException("101", "End date is empty or have wrong pattern");
        if (isNullOrEmpty(contractRequest.getStartDate()) || !date.matcher(contractRequest.getStartDate()).matches())
            throw new ValidationException("102", "Start date is empty or have wrong pattern");
        if (isNullOrEmpty(contractRequest.getSystemName()))
            throw new ValidationException("103", "System name is empty");
        if (Objects.isNull(contractRequest.getAmount()))
            throw new ValidationException("104", "Amount is empty");
        if (Objects.isNull(contractRequest.getAmountType()))
            throw new ValidationException("105", "Amount type is empty");
        if (Objects.isNull(contractRequest.getAmountPeriod()))
            throw new ValidationException("106", "Amount period is empty");
    }

    private boolean isNullOrEmpty(String string) {
        return Objects.isNull(string) || string.isEmpty();
    }
}
