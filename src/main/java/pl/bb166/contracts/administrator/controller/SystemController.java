package pl.bb166.contracts.administrator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.bb166.contracts.administrator.domain.dto.SystemEditRequest;
import pl.bb166.contracts.administrator.domain.entity.SystemEntity;
import pl.bb166.contracts.administrator.exception.ValidationException;
import pl.bb166.contracts.administrator.service.SystemService;

import java.util.List;
import java.util.Objects;

/**
 * Defines all operations relates from System entities control and do validation for Requests.
 * Controller depends SystemService implementation.
 *
 * @author Mateusz Boral
 * @version 0.9.0
 */
@RestController
@RequestMapping("/system")
public class SystemController {


    private SystemService systemService;

    @Autowired
    public void setSystemService(SystemService systemService) {
        this.systemService = systemService;
    }

    /**
     * Method handling get request return all system names in database
     * @return List of all system names
     */
    @GetMapping(value="/fetchNames")
    public List<String> fetchSystemNames() {
        return systemService.getAllSystemNames();
    }

    /**
     * Method handling get request return all system entities in database
     * @return List of System entity witch all properties
     */
    @GetMapping
    public List<SystemEntity> fetchSystems() {
        return systemService.getAllSystems();
    }

    /**
     * Method witch handling system update requests
     * @param system object from request
     * @throws ValidationException when request object has wrong parameters
     */
    @PutMapping
    public void updateSystem(@RequestBody SystemEditRequest system) throws ValidationException {
        validationSystemEditRequest(system);
        systemService.editSystem(system);
    }

    private void validationSystemEditRequest(SystemEditRequest systemEdit) throws ValidationException{
        if (isNullOrEmpty(systemEdit.getName()))
            throw new ValidationException("120", "System name is empty");
        if (isNullOrEmpty(systemEdit.getDescription()))
            throw new ValidationException("121", "System description is empty");
        if (isNullOrEmpty(systemEdit.getOldSystemName()))
            throw new ValidationException("122", "Old system name is empty");
        if (isNullOrEmpty(systemEdit.getTechnologyDescription()))
            throw new ValidationException("123", "Technology description is empty");
    }

    /**
     * Method handling insert system request
     * @param system object form request
     * @throws ValidationException when request object has wrong parameters
     */
    @PostMapping
    public void insertSystem(@RequestBody SystemEntity system) throws ValidationException {
        validationSystemEntity(system);
        systemService.insertSystem(system);
    }

    private void validationSystemEntity(SystemEntity system) throws ValidationException {
        if (isNullOrEmpty(system.getName()))
            throw new ValidationException("130", "System name is empty");
        if (isNullOrEmpty(system.getDescription()))
            throw new ValidationException("131", "System description is empty");
        if (isNullOrEmpty(system.getTechnologyDescription()))
            throw new ValidationException("132", "Technology description is empty");
    }

    private boolean isNullOrEmpty(String string) {
        return Objects.isNull(string) || string.isEmpty();
    }
}