package pl.bb166.contracts.administrator.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller which show general content view
 *
 * @author Mateusz Boral
 * @version 0.9.0
 */
@Controller
public class ViewController {
    @RequestMapping("/")
    public String showView() {
        return "content";
    }
}
