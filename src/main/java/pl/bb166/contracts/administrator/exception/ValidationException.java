package pl.bb166.contracts.administrator.exception;

/**
 * Exception represents problems with input data.
 * Thrown when data have wrong values like date contains letters or something like that.
 * Exception have <code>code</code> property to represent problem identifier.
 *
 * @author Mateusz Boral
 * @version 0.9.0
 */
public class ValidationException extends Exception {
    private String code;

    public ValidationException(String code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }
    public ValidationException(String code, String message) {
        super(message);
        this.code = code;
    }

    public ValidationException(String code, Throwable cause) {
        super(cause);
        this.code = code;
    }

    /**
     * Accessor to get exception identifier
     * @return Exception identifier
     */
    public String getCode() {
        return code;
    }
}
