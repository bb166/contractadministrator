package pl.bb166.contracts.administrator.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.bb166.contracts.administrator.domain.entity.ContractEntity;

import java.util.List;

@Repository
public interface ContractRepository extends CrudRepository<ContractEntity, Long> {
    List<ContractEntity> findByActiveTrue();
    ContractEntity findContractEntityByContractNumberAndActiveTrue(String contractNumber);
    ContractEntity findContractEntityByContractNumber(String contractNumber);
}