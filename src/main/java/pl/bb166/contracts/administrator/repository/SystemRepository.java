package pl.bb166.contracts.administrator.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.bb166.contracts.administrator.domain.entity.SystemEntity;

@Repository
public interface SystemRepository extends CrudRepository<SystemEntity, Long> {
    SystemEntity findSystemEntityByName(String name);
}
