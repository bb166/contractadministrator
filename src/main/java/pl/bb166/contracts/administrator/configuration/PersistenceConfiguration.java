package pl.bb166.contracts.administrator.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

/**
 * Configuration for automatic Spring repositories.
 * Configuration enables transaction management.
 *
 * @author Mateusz Boral
 * @version 0.9.0
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("pl.bb166.contracts.administrator.repository")
public class PersistenceConfiguration {

    /**
     * Key-Value storage contains hibernate properties
     */
    private Properties hibernateProperties;

    /**
     * Default constructor for initialization configuration Key-Value storage.
     * Sets only database dialect currently
     */
    public PersistenceConfiguration() {
        hibernateProperties = new Properties();
        hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
    }

    /**
     * Bean which returning configuration of database
     *
     * @return DataSource objects contains database configuration
     */
    @Bean
    public DataSource getDataSource() {
        DriverManagerDataSource driverDataSource = new DriverManagerDataSource();

        driverDataSource.setDriverClassName("org.postgresql.Driver");
        driverDataSource.setUrl("jdbc:postgresql://localhost:5432/test_database");
        driverDataSource.setUsername("postgres");
        driverDataSource.setPassword("111");

        return driverDataSource;
    }

    /**
     * EntityManagerFactory configuration for Spring automatic repositories
     * provided by Hibernate framework. Bean automatically closing when server
     * will shutdown.
     *
     * @param dataSource contains database configuration
     * @return EntityManagerFactory object configure by DataSource object
     */
    @Bean(destroyMethod = "close")
    public EntityManagerFactory entityManagerFactory(DataSource dataSource) {

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(true);

        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan("pl.bb166.contracts.administrator.domain");
        factory.setDataSource(dataSource);
        factory.afterPropertiesSet();

        return factory.getObject();
    }

    /**
     * Configuration for transaction management
     *
     * @param entityManagerFactory injected by Spring based by configuration
     * @return Transaction manager
     */
    @Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory);
        return transactionManager;
    }
}
