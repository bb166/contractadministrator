package pl.bb166.contracts.administrator.configuration;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * Class represent start point for server of Application
 *
 * @author Mateusz Boral
 * @version 0.9.0
 */
public class WebApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    /**
     * Main path for servlet dispatcher
     * @return Array of path
     */
    @Override
    protected String[] getServletMappings() {
        return new String[] {"/"};
    }

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[] {};
    }

    /**
     *  Return configuration
     *
     * @return Array of classes objects contains application configuration
     */
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[] {
                CoreConfiguration.class,
                ViewResolverConfiguration.class,
                PersistenceConfiguration.class
        };
    }
}