package pl.bb166.contracts.administrator.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;


/**
 * Configuration contains View generation properties
 *
 * @author Mateusz Boral
 * @version 0.9.0
 *
 */
@Configuration
public class ViewResolverConfiguration {

    /**
     * Method defines Apache Tiles configuration object
     * @return Apache Tiles configuration object
     */
    @Bean
    public TilesConfigurer getTilesConfigurer() {
        TilesConfigurer tilesConfigurer = new TilesConfigurer();

        tilesConfigurer.setDefinitions(
                "/WEB-INF/classes/layout/tiles.xml"
        );

        return tilesConfigurer;
    }

    /**
     * Defining Apache Tiles view resolver
     * @return Implementation of ViewResolver interface
     */
    @Bean
    public ViewResolver getTilesViewResolver() {
        return new TilesViewResolver();
    }
}
