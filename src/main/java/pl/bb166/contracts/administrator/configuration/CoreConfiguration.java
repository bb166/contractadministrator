package pl.bb166.contracts.administrator.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Core configuration enable SpringMVC functionality and enable Component find.
 * Contains main configuration of application.
 *
 * @author  Mateusz Boral
 * @version  0.9.0
 */
@Configuration
@EnableWebMvc
@ComponentScan("pl.bb166.contracts.administrator")
public class CoreConfiguration implements WebMvcConfigurer {

    /**
     * Enables view static things contains in applications resource/statics folder
     *
     * @param registry inject by Spring
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("classpath:/statics/");
    }

    /**
     * Configure SimpleDateFormat for date format.
     * note: SimpleDateFormat is not thread safe and construct SimpleDate format instance is slow
     * @return DateFormat bean
     *
     */
    @Bean
    public ThreadLocal<DateFormat> getSimpleDateFormat() {
        return ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd"));
    }

    /**
     * MultipartResolver used to operating file upload functionality
     * @return MultipartResolver bean
     */
    @Bean(name = "multipartResolver")
    public MultipartResolver commonsMultipartResolver() {
        return new CommonsMultipartResolver();
    }

    /**
     * Jedis connection factory for use Redis to cache
     * @return JedisConnectionFactory
     */
    @Bean
    public JedisConnectionFactory jedisConnectionFactory() {
        return new JedisConnectionFactory();
    }

    @Bean
    public RedisTemplate<String, Object> redisTemplate() {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(jedisConnectionFactory());
        return template;
    }
}