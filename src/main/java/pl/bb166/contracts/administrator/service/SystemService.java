package pl.bb166.contracts.administrator.service;

import pl.bb166.contracts.administrator.domain.dto.SystemEditRequest;
import pl.bb166.contracts.administrator.domain.entity.SystemEntity;
import pl.bb166.contracts.administrator.exception.ValidationException;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Implementations represent behavior to control entities of Systems.
 *
 * @author Mateusz Boral
 * @version 0.9.0
 * @see StandardSystemService
 */
@Transactional
public interface SystemService {

    /**
     * Method which get all system names from database
     * @return Strings list of system names
     */
    List<String> getAllSystemNames();

    /**
     * Method  get all system entities form database
     * @return List of system entities
     */
    List<SystemEntity> getAllSystems();

    /**
     * Method which insert system entity to database
     * @param system entity to insert
     * @throws ValidationException - Thrown when model have wrong properties
     */
    void insertSystem(SystemEntity system) throws ValidationException;

    /**
     * Method which update system entity in database
     * @param system request which have data to edit
     * @throws ValidationException - Thrown when model have wrong properties
     */
    void editSystem(SystemEditRequest system) throws ValidationException;
}
