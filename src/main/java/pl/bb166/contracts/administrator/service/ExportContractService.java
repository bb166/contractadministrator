package pl.bb166.contracts.administrator.service;

import org.apache.poi.ss.usermodel.Workbook;
import pl.bb166.contracts.administrator.domain.dto.Statistics;

import javax.transaction.Transactional;

/**
 * Interface defines export behavior Apache POI Workbook to Database
 *
 * @author Mateusz Boral
 * @version 0.9.0
 */
@Transactional
public interface ExportContractService {
    /**
     * Main method to call export
     * @param workbook to export
     * @return Statistics of export
     * @throws Exception when something gone wrong
     */
    Statistics exportToRepository(Workbook workbook) throws Exception;
}