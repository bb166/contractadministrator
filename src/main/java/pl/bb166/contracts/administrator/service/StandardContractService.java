package pl.bb166.contracts.administrator.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.bb166.contracts.administrator.domain.dto.ContractRequest;
import pl.bb166.contracts.administrator.domain.dto.ContractRow;
import pl.bb166.contracts.administrator.domain.entity.ContractEntity;
import pl.bb166.contracts.administrator.domain.entity.SystemEntity;
import pl.bb166.contracts.administrator.exception.ValidationException;
import pl.bb166.contracts.administrator.repository.ContractRepository;
import pl.bb166.contracts.administrator.repository.SystemRepository;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Standard implementation of ContractService.
 * Service use <code>ContractRepository</code> dependency to control ContractEntity entities
 * in database and <code>SystemRepository</code> dependency to control SystemEntity entities.
 * Service use DateFormat packed in ThreadLocal class to convert time.
 *
 * @author Mateusz Boral
 * @version 0.9.0
 * @see ContractService
 */
@Service
public class StandardContractService implements ContractService {

    final Logger logger = Logger.getLogger(getClass());

    private ContractRepository contractRepository;

    private SystemRepository systemRepository;

    private ThreadLocal<DateFormat> dateFormat;

    @Autowired
    public void setContractRepository(ContractRepository contractRepository) {
        this.contractRepository = contractRepository;
    }

    @Autowired
    public void setSystemRepository(SystemRepository systemRepository) {
        this.systemRepository = systemRepository;
    }

    @Autowired
    public void setDateFormal(ThreadLocal<DateFormat> dateFormat) {
        this.dateFormat = dateFormat;
    }

    @Override
    public List<ContractRow> getAllActiveContracts() {
        return mapToContractRowFromRepository(contractRepository.findByActiveTrue());
    }

    @Override
    public List<ContractRow> getAllContracts() {
        return mapToContractRowFromRepository(contractRepository.findAll());
    }

    @Override
    public void createContractFromRequest(ContractRequest model) throws ValidationException, ParseException {
        if (!Objects.isNull(contractRepository.findContractEntityByContractNumber(model.getContractNumber()))) // Check if contract available in database
            throw new ValidationException("1","ContractEntity number available in database");

        SystemEntity system;
        if (Objects.isNull(system = systemRepository.findSystemEntityByName(model.getSystemName()))) // Check if SystemEntity available in database, when SystemEntity available assign instance to
            throw new ValidationException("0","SystemEntity name not available in database");				 // system variable

        ContractEntity contractEntityEntity = createContractFromModel(model); // Call support method to convert Request to entity without call repository for SystemEntity entity
        contractEntityEntity.setSystemEntity(system); 								// Set SystemEntity entity form database in created ContractEntity from request
        system.getContractEntities().add(contractEntityEntity);						// Add ContractEntity to SystemEntity entity

        contractRepository.save(contractEntityEntity);						// Save ContractEntity in database
    }

    @Override
    public void deactivateContract(String id) throws ValidationException {
        ContractEntity contractEntity;
        if (Objects.isNull(contractEntity = contractRepository.findContractEntityByContractNumberAndActiveTrue(id))) // Check if ContractEntity with id parameter available in database, when available assign instance to contract entity
            throw new ValidationException("110", "Not found contracts represent by this id");

        contractEntity.setActive(false); // Deactivate contract
    }

    @Override
    public void updateContractFromRequest(ContractRequest model) throws ValidationException, ParseException {
        logger.debug("Get contractEntity update request with model: " + model);

        ContractEntity contractEntity;
        if (Objects.isNull(contractEntity = contractRepository.findContractEntityByContractNumberAndActiveTrue(model.getContractNumber()))) // Check if ContractEntity available in database
            throw new ValidationException("1", "Cannot find contractEntity in database");

        SystemEntity system;
        if (Objects.isNull(system = systemRepository.findSystemEntityByName(model.getSystemName()))) // Check if SystemEntity available in database
            throw new ValidationException("0", "Cannot find system in database");

        ContractEntity updatedEntity = createContractFromModel(model); // Create ContractEntity entity from request
        updatedEntity.setSystemEntity(system); // Update SystemEntity in entity if user change it

        contractEntity.updateFormEntity(updatedEntity); // Save parameters from request to database
    }

    /**
     * Method create ContractEntity entity from ContractEntity request
     *
     * @param model - Request model from HTTP query
     * @return ContractEntity entity created from request model
     * @throws ParseException - Thrown when date number is in wrong format
     */
    private ContractEntity createContractFromModel(ContractRequest model) throws ParseException {
        ContractEntity contractEntity = new ContractEntity();

        contractEntity.setContractNumber(model.getContractNumber());
        contractEntity.setAmount(model.getAmount());
        contractEntity.setEndDate(dateFormat.get().parse(fixDate(model.getEndDate()))); 		//Fix date because date from request have day number starts by zero.
        contractEntity.setStartDate(dateFormat.get().parse(fixDate(model.getStartDate()))); 	//java.util.Date starts days by one.
        contractEntity.setActive(true);
        contractEntity.setAmountPeriod(model.getAmountPeriod());
        contractEntity.setAmountType(model.getAmountType());

        return contractEntity;
    }

    /**
     * Method repair date by cut last number and append incremental number
     * @param date to repair
     * @return Repaired date
     */
    private String fixDate(String date) {
        return date.substring(0, date.length() - 1)
                + (Character.getNumericValue(date.charAt(date.length()-1)) + 1);
    }

    /**
     * Method map ContractEntity entities from database to ContractRow entities
     *
     * @param entities from database
     * @return List of ContractRow
     */
    private List<ContractRow> mapToContractRowFromRepository(Iterable<ContractEntity> entities) {
        return StreamSupport.stream(entities.spliterator(), false) 	// Create stream based on ContractEntity entites
                .map(ContractRow::new) 								// Map by ContractRow constructor who get ContractEntity entity
                .collect(Collectors.toCollection(LinkedList::new));	// Collect to LinkedList
    }
}