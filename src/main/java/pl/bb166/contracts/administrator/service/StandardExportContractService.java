package pl.bb166.contracts.administrator.service;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.bb166.contracts.administrator.domain.AmountPeriod;
import pl.bb166.contracts.administrator.domain.AmountType;
import pl.bb166.contracts.administrator.domain.ColumnName;
import pl.bb166.contracts.administrator.domain.ContractCellInformation;
import pl.bb166.contracts.administrator.domain.dto.ContractRow;
import pl.bb166.contracts.administrator.domain.dto.Statistics;
import pl.bb166.contracts.administrator.domain.entity.ContractEntity;
import pl.bb166.contracts.administrator.domain.entity.SystemEntity;
import pl.bb166.contracts.administrator.exception.ValidationException;
import pl.bb166.contracts.administrator.repository.ContractRepository;
import pl.bb166.contracts.administrator.repository.SystemRepository;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Pattern;

/**
 * Standard implementation of ExportContractService. Service get implementation of Apache POI Workbook, parse them and insert
 * information to database. Service use SystemRepository and ContractRepository to communication with database.
 * Implementation skips columns and rows which will not pass validation.
 *
 * @author Mateusz Boral
 * @version 0.9.0
 * @see ExportContractService
 */
@Service
public class StandardExportContractService implements ExportContractService {
    final Logger logger = Logger.getLogger(getClass());

    /**
     * Const field will used to column number validation
     */
    private static final int CONTRACT_COLUMN_COUNT = 8;

    /**
     * Pattern objects use to Contract workbook validation.
     */
    private Pattern price = Pattern.compile("^\\d+(\\.\\d{2})$");
    private Pattern contractNumber = Pattern.compile("^\\d+(\\/\\d{4})$");
    private Pattern amountType = Pattern.compile("^(\\bNET\\b)|(\\bBRU\\b)$");
    private Pattern active = Pattern.compile("^(\\btrue\\b)|(\\bfalse\\b)$");
    private Pattern amountPeriod = Pattern.compile("^(\\bMONTH\\b)|(\\bYEAR\\b)$");

    private ContractRepository contractRepository;

    private SystemRepository systemRepository;

    @Autowired
    public void setContractRepository(ContractRepository contractRepository) {
        this.contractRepository = contractRepository;
    }

    @Autowired
    public void setSystemRepository(SystemRepository systemRepository) {
        this.systemRepository = systemRepository;
    }

    @Override
    public Statistics exportToRepository(Workbook workbook) throws Exception {
        Statistics statistics;

        Sheet firstSheet = workbook.getSheetAt(0); // Get first sheet of xlsx file

        Iterator<Row> rows = firstSheet.rowIterator();

        if (!rows.hasNext())
            throw new ValidationException("0", "Empty xlsx file"); // When xlsx haven't even one row, throw exception

        List<ContractCellInformation> headStructure = createHeadStructure(rows.next()); //Get first row and create column structure with used to parse file

        statistics = new Statistics(); // Create empty statistic object

        while (rows.hasNext()) { // Iteration thru workbook
            transformRowToContractRow(headStructure, statistics, rows.next()) // Call method with transform
                    .ifPresent(insertToDatabaseFunction.apply(statistics));     //If method computed value exist,
        }                                                                       //call insert function with returned value.

        return statistics; // Return export statistics
    }

    /**
     * Function will closes statistic value and will insert parse result to database
     */
    private Function<Statistics, Consumer<ContractRow>> insertToDatabaseFunction = statistics -> element -> {
        if (!Objects.isNull(contractRepository.findContractEntityByContractNumber(element.getContractNumber()))) {
            statistics.incrementContractNumber(); //If contract already exist in database, stop insert to database
            return;
        }

        SystemEntity systemEntity;
        if (Objects.isNull(systemEntity = systemRepository.findSystemEntityByName(element.getSystemName()))) {
            statistics.incrementSystemNotAvailable(); // If system not available in database, stop insert to database
            return;
        }

        ContractEntity contractEntity = new ContractEntity(element); // Create Contract based on computed element
        contractEntity.setSystemEntity(systemEntity); // Set found System from database

        contractRepository.save(contractEntity); // Save created element in database
        statistics.incrementSuccessfulAdded(); // Save operation result to statistics
    };

    /**
     * Method with compute ContractRow object from standard Apache POI Row implementation
     * @param headStructure Workbook column structure to parse row
     * @param statistics Work statistics
     * @param row to parse
     * @return Optional of ContractRow
     */
    private Optional<ContractRow> transformRowToContractRow(List<ContractCellInformation> headStructure, Statistics statistics, Row row) {

        ContractRow contractRow = new ContractRow(); // Create empty ContractRow
        Iterator<Cell> cellIterator = row.cellIterator();
        Iterator<ContractCellInformation> informationIterator = headStructure.iterator(); // Create iterator to iterate thru column structure
        ContractCellInformation iteration = informationIterator.next(); // Get first row of column structure

        for (int i = 0; cellIterator.hasNext(); i++) { // Iterate thru cells of row
            if (i < iteration.getAbsoluteNumber()) { // Check if absolute number of column structure is grater than cell number. If it is true, skip this cell
                cellIterator.next();
                continue;
            }
            Cell cell = cellIterator.next();
            ColumnName columnName = iteration.getColumnName();

            switch (columnName) { // Switch structure used to find column type and parse to ContractRow property
                case AMOUNT: {
                    cell.setCellType(CellType.STRING);
                    String cellValue = cell.getStringCellValue();
                    if (price.matcher(cellValue).matches()) {
                        contractRow.setAmount(new BigDecimal(cellValue));
                    } else {
                        statistics.incrementWrongAmount();
                        return Optional.empty();
                    }
                    break;
                }
                case TO_DATE: {
                    cell.setCellType(CellType.NUMERIC);
                    if (DateUtil.isCellDateFormatted(cell)) {
                        Date cellValue = cell.getDateCellValue();
                        contractRow.setEndDate(cellValue);
                    } else {
                        statistics.incrementWrongEndDateFormat();
                        return Optional.empty();
                    }
                    break;

                }
                case FROM_DATE: {
                    cell.setCellType(CellType.NUMERIC);
                    if (DateUtil.isCellDateFormatted(cell)) {
                        Date cellValue = cell.getDateCellValue();
                        contractRow.setStartDate(cellValue);
                    } else {
                        statistics.incrementWrongStartDateFormat();
                        return Optional.empty();
                    }
                    break;
                }
                case ORDER_NUMBER: {
                    String cellValue = cell.getStringCellValue();
                    if (contractNumber.matcher(cellValue).matches()) {
                        contractRow.setContractNumber(cellValue);
                    } else {
                        statistics.incrementWrongOrderNumber();
                        return Optional.empty();
                    }
                    break;
                }
                case AMOUNT_TYPE: {
                    String cellValue = cell.getStringCellValue();
                    if (amountType.matcher(cellValue).matches()) {
                        contractRow.setAmountType(AmountType.forImport(cellValue));
                    } else {
                        statistics.incrementWrongAmountType();
                        return Optional.empty();
                    }
                    break;
                }
                case ACTIVE: {
                    cell.setCellType(CellType.STRING);
                    String cellValue = cell.getStringCellValue();
                    if (active.matcher(cellValue).matches()) {
                        contractRow.setActive(Boolean.valueOf(cellValue));
                    } else {
                        statistics.incrementWrongActive();
                        return Optional.empty();
                    }
                    break;
                }
                case AMOUNT_PERIOD: {
                    String cellValue = cell.getStringCellValue();
                    if (amountPeriod.matcher(cellValue).matches()) {
                        contractRow.setAmountPeriod(AmountPeriod.getByNameForImport(cellValue));
                    } else {
                        statistics.incrementWrongAmountPeriod();
                        return Optional.empty();
                    }
                    break;
                }
                case SYSTEM_NAME: {
                    String cellValue = cell.getStringCellValue();
                    contractRow.setSystemName(cellValue);
                }
            }

            if (informationIterator.hasNext()) {  // If end of column structure, exit row iteration
                iteration = informationIterator.next();
            } else {
                break;
            }
        }

        return Optional.of(contractRow);
    }

    /**
     * Method to create head structure of file
     * @param headRow First row in file
     * @return Column structure
     * @throws ValidationException when column structure not valid
     */
    private List<ContractCellInformation> createHeadStructure(final Row headRow) throws ValidationException {
        List<ContractCellInformation> cellInformations = new LinkedList<>(); // Create empty list to will contains CellInformation

        Iterator<Cell> cellIterator = headRow.cellIterator(); // Create cell iterator from head row

        for (int i = 0; cellIterator.hasNext(); i++) { // Iteration thru head row
            String cellName = cellIterator.next().getStringCellValue().trim(); // Get string value from cell and remove white characters

            if(!checkPredicateLazy(ColumnName.COLUMN_NAMES, checkAvailabilityNameInColumnName.apply(cellName))) // Check if column is valid column name for Contract
                continue;

            if(checkPredicateLazy(cellInformations, checkAvailabilityInProcessedColumn.apply(cellName)))
                throw new ValidationException("1", "Duplicate head cell name");

            Function<Integer, Consumer<ColumnName>> integerComposition =
                    integer -> element -> cellInformations.add(new ContractCellInformation(integer, element));

            ColumnName.getForName(cellName).ifPresent(integerComposition.apply(i)); // If column is valid, add it to structure
        }

        if (cellInformations.size() < CONTRACT_COLUMN_COUNT)
            throw new ValidationException("2", "Wrong head xlsx structure");

        return cellInformations;
    }

    private Function<String, Function<ContractCellInformation, Boolean>> checkAvailabilityInProcessedColumn =
            value -> information ->
                    information
                            .getColumnName()
                            .getNativeName()
                            .equals(value);


    private static Function<String, Function<ColumnName, Boolean>> checkAvailabilityNameInColumnName =
            value -> name ->
                        name
                            .getNativeName()
                            .equals(value);

    private static <T> boolean checkPredicateLazy(Collection<T> collection, Function<T, Boolean> predicate) {
        for (T t : collection) {
            if (predicate.apply(t)) {
                return true;
            }
        }
        return false;
    }
}