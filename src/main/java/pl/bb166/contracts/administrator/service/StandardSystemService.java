package pl.bb166.contracts.administrator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.bb166.contracts.administrator.domain.dto.SystemEditRequest;
import pl.bb166.contracts.administrator.domain.entity.SystemEntity;
import pl.bb166.contracts.administrator.exception.ValidationException;
import pl.bb166.contracts.administrator.repository.SystemRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Standard implementation of SystemService which use systemRepository dependency
 * to operate on database
 *
 * @author Mateusz Boral
 * @version 0.9.0
 * @see SystemService
 */
@Service
public class StandardSystemService implements SystemService {

    private SystemRepository systemRepository;

    @Autowired
    public void setSystemRepository(SystemRepository systemRepository) {
        this.systemRepository = systemRepository;
    }

    @Override
    public void insertSystem(SystemEntity system) throws ValidationException {
        if (!Objects.isNull(systemRepository.findSystemEntityByName(system.getName())))
            throw new ValidationException("0", "System available in database");

        systemRepository.save(system);
    }

    @Override
    public List<SystemEntity> getAllSystems() {
        return StreamSupport
                .stream(systemRepository.findAll().spliterator(), false)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public List<String> getAllSystemNames() {
        return StreamSupport.stream(systemRepository.findAll().spliterator(),false)
                .map(SystemEntity::getName)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public void editSystem(SystemEditRequest system) throws ValidationException {

        if (!system.getOldSystemName().equals(system.getName()) &&
                !Objects.isNull(systemRepository.findSystemEntityByName(system.getName())))
            throw new ValidationException("0","System in this name is available in database");

        SystemEntity systemEntity;
        if (Objects.isNull(systemEntity = systemRepository.findSystemEntityByName(system.getOldSystemName())))
            throw new ValidationException("1", "Cannot find system name");

        systemEntity.setName(system.getName());
        systemEntity.setTechnologyDescription(system.getTechnologyDescription());
        systemEntity.setDescription(system.getDescription());
    }
}