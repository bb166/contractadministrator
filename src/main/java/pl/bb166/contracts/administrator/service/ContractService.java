package pl.bb166.contracts.administrator.service;

import pl.bb166.contracts.administrator.domain.dto.ContractRequest;
import pl.bb166.contracts.administrator.domain.dto.ContractRow;
import pl.bb166.contracts.administrator.exception.ValidationException;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.util.List;

/**
 * Interface defines methods to control data in database.
 * Main task to implementation of this interface:
 * <ul>
 * 	<li>Process data from requests and update database by them</li>
 * 	<li>Gets information from database, prepare it and return in form ready to send to requester</li>
 * </ul>
 * All operation defined in this interface are transactional
 *
 * @author Mateusz Boral
 * @version 0.9.0
 * @see StandardContractService
 */
@Transactional
public interface ContractService {

    /**
     * Get contracts with active property set on <code>true</code>
     *
     * @return List of ContractRow
     */
    List<ContractRow> getAllActiveContracts();

    /**
     * Get all contracts from database
     *
     * @return List of ContractRow
     */
    List<ContractRow> getAllContracts();

    /**
     * Create contract in database based on data give in request
     *
     * @param model - Request from client
     * @throws ValidationException - Thrown when model have wrong properties
     * @throws ParseException - Thrown when date is in wrong format
     */
    void createContractFromRequest(ContractRequest model) throws ValidationException, ParseException;

    /**
     * Update contract in database based on data give in request
     *
     * @param model - Model to update row in database
     * @throws ValidationException - Thrown when model have wrong properties
     * @throws ParseException - Thrown when date is in wrong format
     */
    void updateContractFromRequest(ContractRequest model) throws ValidationException, ParseException;

    /**
     * Change active value on false.
     * Row identify by contract number
     *
     * @param id - value of contract number property
     * @throws ValidationException when contract of given number not available in database
     */
    void deactivateContract(String id) throws ValidationException;
}