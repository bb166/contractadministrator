$(document).ready(function () {
    var successDialog = $('#successDialog');

    successDialog.dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            Ok: function() {
                $(this).dialog("close");
                activeContractTable.DataTable().ajax.reload();
                systemTable.DataTable().ajax.reload();
            }
        }
    });

    var activeContractTable = $('#activeContractsTable'); // Declare activeContractTable handle

    activeContractTable.DataTable({ // Create table for handle
        ajax: {
            url: context + "/contract/active",
            method: 'GET',
            dataSrc: function (data) {
                return data;
            }
        },
        columns: [
            {"data": "contractNumber"},
            {"data": "systemName"},
            {"data": "startDate"},
            {"data": "endDate"},
            {"data": "amount", "render": $.fn.dataTable.render.number(',', '.', 2)},
            {"data": "amountType"},
            {"data": "amountPeriod"},
            {"defaultContent": "<button class='edit ui-button ui-corner-all'>Edytuj</button><button class='deactivate ui-button ui-corner-all'>Deaktywuj</button>"}
        ]
    });

    $('#activeContractsTable tbody').on('click', '.edit', function () {
        var data = activeContractTable.DataTable().row($(this).parents('tr')).data();

        editContractNumberField.text("Numer kontraktu: "+data.contractNumber);
        editContractNumberField.val(data.contractNumber)
        editContractSystemName.val(data.systemName);
        editContractStartDate.val(data.startDate);
        editContractEndDate.val(data.endDate);
        editContractAmount.val(data.amount);
        editContractAmountPeriod.val(data.amountPeriod);
        editContractAmountType.val(data.amountType);

        editContractAmount.currency({
            thousands: "",
            decimal: ".",
            decimals: 2,
            hidePrefix: true,
            hidePostfix: true
        });

        editContractDialog.dialog('open');
    });

    $('#activeContractsTable tbody').on( 'click', '.deactivate', function () {
        var data = activeContractTable.DataTable().row( $(this).parents('tr') ).data();

        $.ajax({
            type: "DELETE",
            url: context + "/contract/?id=" + data.contractNumber,

            success: function () {
                activeContractTable.DataTable().ajax.reload();
            }
        });
    });

    var createContractButton            = $('#createContractButton'),
        insertContractDialog            = $('#insertContractDialog'),
        insertContractForm              = $('#insertContractForm'),
        insertContractValidationTips    = $('#insertContractValidationTips'),
        insertContractNumber            = $('#insertContractNumber'),
        insertEndDate                   = $('#insertEndDate'),
        insertStartDate                 = $('#insertStartDate'),
        insertAmount       	            = $('#insertAmount'),
        insertContractSystemName        = $('#insertContractSystemName'),
        insertAmountPeriod	            = $('#insertAmountPeriod'),
        insertAmountType		        = $('#insertAmountType');

    var insertContractFields =
        $([])
            .add(insertContractNumber)
            .add(insertEndDate)
            .add(insertStartDate)
            .add(insertAmount)
            .add(insertContractSystemName)
            .add(insertAmountPeriod)
            .add(insertAmountType);

    insertContractDialog.dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            "Create": createAndSendContract,
            "Cancel": function() {
                insertContractDialog.dialog("close");
            }
        },
        close: function() {
            insertContractForm[0].reset();
            insertContractFields.removeClass('ui-state-error');
            insertContractValidationTips.text("");
            insertContractValidationTips.removeClass("ui-state-highlight");
        }
    });

    function createAndSendContract() {
        if (insertContractValidation()) {
            var content = {};
            content.contractNumber  = insertContractNumber.val();
            content.startDate       = insertStartDate.val();
            content.endDate         = insertEndDate.val();
            content.amount       	= insertAmount.val();
            content.systemName      = insertContractSystemName.val();
            content.amountType		= insertAmountType.find('option:selected').text();
            content.amountPeriod	= insertAmountPeriod.find('option:selected').text();
            console.log(JSON.stringify(content));
            $.ajax({
                type: "POST",
                url: context + "/contract",
                contentType: 'application/json',
                data: JSON.stringify(content),
                success: function () {
                    insertContractDialog.dialog("close");
                    successDialog.dialog("open");
                },
                error: function (xhr) {
                    console.log(JSON.parse(xhr.responseText));
                    var message = JSON.parse(xhr.responseText);
                    if (message.code === '103' || message.code === '0') {
                        insertContractSystemName.addClass("ui-state-error");
                        insertContractUpdateTips("Podana nazwa systemu nie jest dostępna");
                    } else if (message.code === '1') {
                        insertContractNumber.addClass("ui-state-error");
                        insertContractUpdateTips("Kontrakt o podanym numerze jest już dostępny w bazie danych");
                    }
                }
            });
        }
    }

    function insertContractUpdateTips(t) {
        insertContractValidationTips
            .text(t)
            .addClass("ui-state-highlight");
        setTimeout(function() {
            insertContractValidationTips.removeClass("ui-state-highlight", 2000);
        }, 750 );
    }

    function insertContractValidation() {
        insertContractFields.removeClass('ui-state-error');

        var result =
            insertContractCheckRegex(insertContractNumber, /^\d+(\/\d{4})$/, "Nieprawidłowy numer kontraktu")
            &&  insertContractCheckRegex(insertAmount, /^\d+(\.\d{2})$/, "Nieprawidłowy format wartości")
            &&  insertContractCheckDates()
            &&  lengthHighThanZero();

        return result;
    }

    function insertContractCheckDates() {
        var sd = new Date(insertStartDate.val());
        var ed = new Date(insertEndDate.val());

        var result = true;
        if (sd > ed) {
            insertStartDate.addClass("ui-state-error");
            insertEndDate.addClass("ui-state-error");
            insertContractUpdateTips("Data rozpoczęcia musi być mniejsza od daty zakończenia");

            result = false;
        }
        return result;
    }

    function lengthHighThanZero() {
        var result = true;

        if (insertStartDate.val().length === 0) {
            insertContractUpdateTips("Pole daty rozpoczęcia jest puste");
            insertStartDate.addClass("ui-state-error");
            result = false;
        } else if (insertEndDate.val().length === 0) {
            insertContractUpdateTips("Pole daty zakończenia jest puste");
            insertEndDate.addClass("ui-state-error");
            result = false;
        }

        return result;
    }

    function insertContractCheckRegex(o, regex, n) {
        if (!(regex.test(o.val()))) {
            insertContractUpdateTips(n);
            o.addClass("ui-state-error");
            return false;
        } else
            return true;
    }

    createContractButton.click(function() {
        insertContractDialog.dialog('open');
    });

    var editContractDialog          = $('#editContractDialog'),
        editContractForm            = $('#editContractForm'),
        editContractValidationTips  = $('#editContractValidationTips'),
        editContractNumberField     = $('#editContractNumberField'),
        editContractEndDate		    = $('#editContractEndDate'),
        editContractStartDate		= $('#editContractStartDate'),
        editContractAmount			= $('#editContractAmount'),
        editContractSystemName	    = $('#editContractSystemName'),
        editContractAmountPeriod	= $('#editContractAmountPeriod'),
        editContractAmountType		= $('#editContractAmountType');

    var editContractFields =
        $([])
            .add(editContractEndDate)
            .add(editContractStartDate)
            .add(editContractAmount)
            .add(editContractSystemName)
            .add(editContractAmountPeriod)
            .add(editContractAmountType);

    editContractDialog.dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            "Edytuj": editAndSendContract,
            "Anuluj": function() {
                editContractDialog.dialog("close");
            }
        },
        close: function() {
            editContractForm[0].reset();
            editContractFields.removeClass('ui-state-error');
            editContractValidationTips.text("");
            editContractValidationTips.removeClass("ui-state-highlight");
        }
    });

    function editAndSendContract() {
        if (editContractValidation()) {
            var content = {};
            content.contractNumber  = editContractNumberField.val();
            content.startDate	   	= editContractStartDate.val();
            content.endDate		   	= editContractEndDate.val();
            content.amount	   		= editContractAmount.val();
            content.systemName	   	= editContractSystemName.val();
            content.amountType		= editContractAmountType.find('option:selected').text();
            content.amountPeriod	= editContractAmountPeriod.find('option:selected').text();
            console.log(JSON.stringify(content));
            $.ajax({
                type: "PUT",
                url: context + "/contract",
                contentType: 'application/json',
                data: JSON.stringify(content),
                success: function () {
                    editContractDialog.dialog("close");
                    successDialog.dialog("open");
                },
                error: function (xhr) {
                    console.log(xhr.responseText);
                    var message = JSON.parse(xhr.responseText);
                    if (message.code === '0' || message.code === '103') {
                        editContractSystemName.addClass("ui-state-error");
                        updateEditContractValidationTips("System o podanej nazwie nie jest dostępny w bazie danych");
                    } else if (message.code === '1') {
                        console.log('Contract name not available in database')
                    }
                }
            });
        }
    }

    function editContractValidation() {
        editContractFields.removeClass('ui-state-error');

        var result =
            editContractCheckRegex(editContractAmount, /^\d+(\.\d{2})$/, "Nieprawidłowy format wartości")
            && editContractCheckDates()
            && lengthHighThanZeroEdit();

        return result;
    }

    function lengthHighThanZeroEdit() {
        var result = true;

        if (editContractStartDate.val().length === 0) {
            editContractValidationTips("Pole daty rozpoczęcia jest puste");
            editContractStartDate.addClass("ui-state-error");
            result = false;
        } else if (editContractEndDate.val().length === 0) {
            updateEditContractValidationTips("Pole daty zakończenia jest puste");
            editContractEndDate.addClass("ui-state-error");
            result = false;
        }

        return result;
    }

    function editContractCheckRegex(o, regex, n) {
        if (!(regex.test(o.val()))) {
            updateEditContractValidationTips(n);
            o.addClass('ui-state-error');
            return false;
        } else
            return true;
    }

    function editContractCheckDates() {
        var sd = new Date(editContractStartDate.val());
        var ed = new Date(editContractEndDate.val());

        var valid = true;
        if (sd > ed) {
            editContractStartDate.addClass("ui-state-error");
            editContractEndDate.addClass("ui-state-error");
            updateEditContractValidationTips("Data rozpoczęcia musi być mniejsza od daty zakończenia");

            valid = false;
        }
        return valid;
    }

    function updateEditContractValidationTips(t) {
        editContractValidationTips
            .text(t)
            .addClass("ui-state-highlight");
        setTimeout(function() {
            editContractValidationTips.removeClass("ui-state-highlight", 2000);
        }, 750 );
    }

    var allContractsTable = $('#allContractTable'); // Declare allContractsTable handle

    allContractsTable.DataTable({ // Create table for handle
        ajax: {
            url: context + "/contract",
            method: 'GET',
            dataSrc: function (data) {
                return data;
            }
        },
        columns: [
            {"data": "contractNumber"},
            {"data": "systemName"},
            {"data": "startDate"},
            {"data": "endDate"},
            {"data": "amount", "render": $.fn.dataTable.render.number(',', '.', 2)},
            {"data": "amountType"},
            {"data": "amountPeriod"},
            {
                "data": "active", "render": function (data) {
                    return data ? "Tak" : "Nie";
                }
            }
        ]
    });

    var systemTable = $('#systemTable');

    systemTable.DataTable({
        ajax: {
            url: context + "/system",
            method: "GET",
            dataSrc: function (data) {
                return data;
            }
        },
        columns: [
            {"data": "name"},
            {"data": "description"},
            {"data": "technologyDescription"},
            {"defaultContent": "<button class='edit ui-button ui-corner-all'>Edytuj</button>"}
        ]
    });

    systemTable.parents('div.dataTables_wrapper').first().hide();

    var editSystemDialog    = $('#editSystemDialog'),
        editSystemForm      = $('#editSystemForm'),
        editSystemTips      = $('#editSystemTips'),
        editSystemName      = $('#editSystemName'),
        editDescription     = $('#editDescription'),
        editTechDescription = $('#editTechDescription'),
        oldSystemName;

    var editSystemFields =
        $([])
            .add(editSystemName)
            .add(editDescription)
            .add(editTechDescription);

    editSystemDialog.dialog({
        autoOpen:false,
        modal: true,
        buttons: {
            "Edytuj": editAndSendSystem,
            "Anuluj": function () {
                editSystemDialog.dialog('close');
            }
        },
        close: function () {
            editSystemForm[0].reset();
            editSystemTips.text("");
            editSystemFields.removeClass('ui-state-error');
            editSystemTips.removeClass("ui-state-highlight");
        }
    });

    function editAndSendSystem() {
        if (editSystemValidation()) {
            var content = {};

            content.oldSystemName           = oldSystemName;
            content.name 			        = editSystemName.val();
            content.description		        = editDescription.val();
            content.technologyDescription   = editTechDescription.val();
            console.log(JSON.stringify(content));
            $.ajax({
                type: "PUT",
                url: context + "/system",
                contentType: 'application/json',
                data: JSON.stringify(content),
                success: function () {
                    editSystemDialog.dialog("close");
                    successDialog.dialog("open");
                    refreshAutocomplete();
                },
                error: function (xhr) {
                    console.log(xhr.responseText);
                    var message = JSON.parse(xhr.responseText);
                    if (message.code === '0') {
                        editSystemName.addClass("ui-state-error");
                        updateEditSystemTips("System o podanej nazwie jest już dostępny");
                    }
                }
            });
        }
    }

    function editSystemValidation() {
        editSystemFields.removeClass('ui-state-error');
        var valid = true;

        if (editSystemName.val().length === 0) {
            updateEditSystemTips("Pole nazwy systemu jest puste");
            editSystemName.addClass("ui-state-error");
            valid = false;
        } else if (editDescription.val().length === 0) {
            updateEditSystemTips("Pole opisu jest puste");
            editDescription.addClass("ui-state-error");
            valid = false;
        } else if (editTechDescription.val().length === 0) {
            updateEditSystemTips("Pole opisu technologicznego jest puste");
            editTechDescription.addClass('ui-state-error');
            valid = false;
        }

        return valid;
    }

    function updateEditSystemTips(t) {
        editSystemTips
            .text(t)
            .addClass("ui-state-highlight");
        setTimeout(function() {
            editSystemTips.removeClass("ui-state-highlight", 2000);
        }, 750 );
    }

    $('#systemTable tbody').on('click', '.edit', function () {
        var data = systemTable.DataTable().row($(this).parents('tr')).data();

        oldSystemName = data.name;

        editSystemName.val(oldSystemName);
        editDescription.val(data.description);
        editTechDescription.val(data.technologyDescription);

        editSystemDialog.dialog('open');
    });

    var createSystemDialog          = $('#createSystemDialog'),
        createSystemForm            = createSystemDialog.find('#createSystemForm'),
        createSystemButton 			= $('#createSystemButton'),
        createSystemName   			= $('#createSystemName'),
        createSystemDescription 	= $('#createDescription'),
        createSystemTechDescription = $('#createTechDescription'),
        createSystemTips			= $('#createSystemTips');

    var createSystemFields =
        $([])
            .add(createSystemName)
            .add(createSystemDescription)
            .add(createSystemTechDescription);

    createSystemButton.click(function(){
        createSystemDialog.dialog('open');
    });

    createSystemButton.hide();

    createSystemDialog.dialog({
        autoOpen:false,
        modal: true,
        buttons: {
            "Create": createAndSendSystem,
            "Cancel": function () {
                createSystemDialog.dialog('close');
            }
        },
        close: function (){
            createSystemForm[0].reset();
            createSystemTips.text("");
            createSystemFields.removeClass('ui-state-error');
            createSystemTips.removeClass("ui-state-highlight");
        }
    });

    function createAndSendSystem() {
        if (createSystemValidation()) {
            var content = {};

            content.name 			        = createSystemName.val();
            content.description		        = createSystemDescription.val();
            content.technologyDescription   = createSystemTechDescription.val();
            console.log(JSON.stringify(content));
            $.ajax({
                type: "POST",
                url: context + "/system",
                contentType: 'application/json',
                data: JSON.stringify(content),
                success: function () {
                    createSystemDialog.dialog("close");
                    successDialog.dialog("open");
                    refreshAutocomplete();
                },
                error: function (xhr) {
                    console.log(xhr.responseText);
                    var message = JSON.parse(xhr.responseText);
                    if (message.code === '0') {
                        createSystemName.addClass("ui-state-error");
                        updateCreateSystemTips("System o podanej nazwie jest już dostępny w bazie");
                    }
                }
            });
        }
    }

    function refreshAutocomplete() {
        $.ajax({
            url: context + "/system/fetchNames"
        }).done(function (data) {
            insertContractSystemName.autocomplete({
                source: data,
                dataType: "json"
            });
            editContractSystemName.autocomplete({
                source: data,
                dataType: 'json'
            });
        });
    }

    refreshAutocomplete();

    function createSystemValidation() {
        createSystemFields.removeClass('ui-state-error');
        var valid = true;

        if (createSystemName.val().length === 0) {
            updateCreateSystemTips("Pole nazwy systemu jest puste");
            createSystemName.addClass("ui-state-error");
            valid = false;
        } else if (createSystemDescription.val().length === 0) {
            updateCreateSystemTips("Pole opisu jest puste");
            createSystemDescription.addClass("ui-state-error");
            valid = false;
        } else if (createSystemTechDescription.val().length === 0) {
            updateCreateSystemTips("Pole opisu technologicznego jest puste");
            createSystemTechDescription.addClass('ui-state-error');
            valid = false;
        }

        return valid;
    }

    function updateCreateSystemTips(t) {
        createSystemTips
            .text(t)
            .addClass("ui-state-highlight");
        setTimeout(function() {
            createSystemTips.removeClass("ui-state-highlight", 2000);
        }, 750 );
    }

    var exportButton    = $('#exportButton'),
        exportDialog    = $('#exportDialog'),
        exportForm      = exportDialog.find('#exportForm'),
        exportTips      = $('#exportTips'),
        exportInput     = $('#exportInput');


    exportButton.show();

    exportButton.click(function () {
        exportDialog.dialog('open');
    });

    exportDialog.dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            "Importuj": importContracts,
            "Anuluj": function () {
                exportDialog.dialog('close');
            }
        },
        close: function () {
            exportForm[0].reset();
            exportTips.text("");
            exportInput.removeClass('ui-state-error');
            exportTips.removeClass("ui-state-highlight");
        }
    });

    var exportStatisticsDialog  = $('#exportStatisticsDialog'),
        successfulAdded 	    = $('#successfulAdded'),
        alreadyAvailable	    = $('#alreadyAvailable'),
        systemNotAvailable	    = $('#systemNotAvailable'),
        wrongAmount			    = $('#wrongAmount'),
        wrongStartDate		    = $('#wrongStartDate'),
        wrongEndDate		    = $('#wrongEndDate'),
        wrongOrderNumber	    = $('#wrongOrderNumber'),
        wrongAmountType		    = $('#wrongAmountType'),
        wrongAmountPeriod	    = $('#wrongAmountPeriod'),
        wrongActive			    = $('#wrongActive');

    exportStatisticsDialog.dialog({
        autoOpen:false,
        modal: true,
        buttons: {
            "Ok": function () {
                exportStatisticsDialog.dialog('close');
            }
        },
        close: function (){
            activeContractTable.DataTable().ajax.reload();
            exportForm[0].reset();
            exportTips.text("");
            exportInput.removeClass('ui-state-error');
            exportTips.removeClass("ui-state-highlight");
        }
    });

    function importContracts() {
        var formData = new FormData();
        var file	= $('input:file')[0].files[0];

        formData.append('file', file);

        $.ajax({
            url: context + '/contract/importFile',
            type: 'POST',
            contentType: false,
            data: formData,
            processData: false,
            success: function(data) {
                exportDialog.dialog('close');
                successfulAdded.text(data.successfulAdded);
                alreadyAvailable.text(data.contractNumberAvailableAlready);
                systemNotAvailable.text(data.systemNotAvailable);
                wrongAmount.text(data.wrongAmount);
                wrongStartDate.text(data.wrongStartDateFormat);
                wrongEndDate.text(data.wrongEndDateFormat);
                wrongOrderNumber.text(data.wrongOrderNumber);
                wrongAmountType.text(data.wrongAmountType);
                wrongAmountPeriod.text(data.wrongAmountPeriod);
                wrongActive.text(data.wrongActive);
                exportStatisticsDialog.dialog('open');
            },
            error: function(data) {
                console.log(data.responseText);
                var message = JSON.parse(data.responseText);
                exportInput.addClass("ui-state-error");
                updateExportTips(message.message);

            }
        });
    }

    function updateExportTips(t) {
        exportTips
            .text(t)
            .addClass("ui-state-highlight");
        setTimeout( function() {
            exportTips.removeClass("ui-state-highlight", 2000);
        }, 750 );
    }

    allContractsTable.parents('div.dataTables_wrapper').first().hide();

    var about = $('#about');

    about.dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            Ok: function() {
                $(this).dialog("close");
            }
        }
    });

    $('#author').click(function() {
        about.dialog("open")
    });

    $('#activeContracts').click(function() {
        createContractButton.show();
        exportButton.show();
        systemTable.parents('div.dataTables_wrapper').first().show();
        activeContractTable.DataTable().ajax.reload();
        activeContractTable.parents('div.dataTables_wrapper').first().show();
        allContractsTable.parents('div.dataTables_wrapper').first().hide();
        systemTable.parents('div.dataTables_wrapper').first().hide();
        createSystemButton.hide();
    });

    $('#allContracts').click(function() {
        allContractsTable.DataTable().ajax.reload();
        allContractsTable.parents('div.dataTables_wrapper').first().show();
        createContractButton.hide();
        activeContractTable.parents('div.dataTables_wrapper').first().hide();
        systemTable.parents('div.dataTables_wrapper').first().hide();
        createSystemButton.hide();
        exportButton.hide();
    });

    $('#allSystems').click(function() {
        systemTable.parents('div.dataTables_wrapper').first().show();
        systemTable.DataTable().ajax.reload();
        createContractButton.hide();
        activeContractTable.parents('div.dataTables_wrapper').first().hide();
        allContractsTable.parents('div.dataTables_wrapper').first().hide();
        createSystemButton.show();
        exportButton.hide();
    });
});