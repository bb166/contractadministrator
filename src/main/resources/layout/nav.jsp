<%@ page contentType="text/html; charset=UTF-8" %>
<h1>Contracts administrator</h1>
<nav>
    <button class="ui-button ui-widget ui-corner-all" id="activeContracts">Aktywne kontrakty</button>
    <button class="ui-button ui-widget ui-corner-all" id="allContracts">Wszystkie kontrakty</button>
    <button class="ui-button ui-widget ui-corner-all" id="allSystems">Systemy</button>
    <button class="ui-button ui-widget ui-corner-all" id="author">O programie</button>
</nav>