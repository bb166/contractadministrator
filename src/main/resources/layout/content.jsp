<%@ page contentType="text/html; charset=UTF-8"%>
<table id="activeContractsTable" class="display">
    <thead>
        <tr>
            <th>Numer kontraktu</th>
            <th>Nazwa systemu</th>
            <th>Data rozpoczęcia</th>
            <th>Data zakończenia</th>
            <th>Wartość</th>
            <th>Typ</th>
            <th>Okres</th>
            <th></th>
        </tr>
    </thead>
</table>

<div id="insertContractDialog" title="Utwórz kontrakt">
    <p id="insertContractValidationTips"></p>
    <form id="insertContractForm">
        <label for="insertContractNumber" class="lab">Numer kontraktu:</label>
        <input type="text" name="insertContractNumber" id="insertContractNumber"
               class="text ui-widget-content ui-corner-all" maxlength="32">
        <label for="insertStartDate" class="lab">Data rozpoczęcia:</label>
        <input type="date" name="insertStartDate" id="insertStartDate" class="text ui-widget-content ui-corner-all">
        <label for="insertEndDate" class="lab">Data zakończenia:</label>
        <input type="date" name="insertEndDate" id="insertEndDate" class="text ui-widget-content ui-corner-all">
        <label for="insertAmount" class="lab">Wartość:</label>
        <input type="text" name="insertAmount" id="insertAmount" class="text ui-widget-content ui-corner-all">
        <label for="insertAmountType" class="lab">Typ:</label>
        <select name="insertAmountType" id="insertAmountType" class="ui-widget-content ui-corner-all">
            <option value="Netto" selected>Netto</option>
            <option value="Brutto">Brutto</option>
        </select>
        <label for="insertAmountPeriod" class='lab'>Okres:</label>
        <select name="insertAmountPeriod" id="insertAmountPeriod" class="ui-widget-content ui-corner-all">
            <option value="Miesieczni" selected>Miesiecznie</option>
            <option value="Rocznie">Rocznie</option>
        </select>
        <label for="insertContractSystemName" class="lab">Nazwa systemu:</label>
        <input type="text" name="insertContractSystemName" id="insertContractSystemName"
               class="text ui-widget-content ui-corner-all">
    </form>
</div>

<div id="editContractDialog" title="Edycja kontraktu">
    <p id="editContractValidationTips"></p>
    <p id="editContractNumberField">
    <p>
    <form id="editContractForm">
        <label for="editContractStartDate" class="lab">Data rozpoczęcia:</label>
        <input type="date" name="editContractStartDate" id="editContractStartDate"
               class="text ui-widget-content ui-corner-all" maxlength="32">
        <label for="editContractEndDate" class="lab">Data zakończenia:</label>
        <input type="date" name="editContractEndDate" id="editContractEndDate"
               class="text ui-widget-content ui-corner-all">
        <label for="editContractAmount" class="lab">Wartość:</label>
        <input type="text" name="editContractAmount" id="editContractAmount"
               class="text ui-widget-content ui-corner-all">
        <label for="editContractAmountType" class="lab">Typ:</label>
        <select name="editContractAmountType" id="editContractAmountType" class="ui-widget-content ui-corner-all">
            <option value="Netto" selected>Netto</option>
            <option value="Brutto">Brutto</option>
        </select>
        <label for="editContractAmountPeriod" class='lab'>Okres:</label>
        <select name="editContractAmountPeriod" id="editContractAmountPeriod" class="ui-widget-content ui-corner-all">
            <option value="Miesiecznie" selected>Miesiecznie</option>
            <option value="Rocznie">Rocznie</option>
        </select>
        <label for="editContractSystemName" class="lab">Nazwa systemu:</label>
        <input type="text" name="editContractSystemName" id="editContractSystemName"
               class="text ui-widget-content ui-corner-all">
    </form>
</div>

<button id="createContractButton" class="ui-button ui-widget ui-corner-all">Stwórz kontrakt</button>
<button id="exportButton" class="ui-button ui-widget ui-corner-all">Importuj</button>

<table id="allContractTable" class="display" style="width: 100%; margin: auto;">
    <thead>
        <tr>
            <th>Numer kontraktu</th>
            <th>Nazwa systemu</th>
            <th>Data rozpoczęcia</th>
            <th>Data zakończenia</th>
            <th>Wartość</th>
            <th>Typ</th>
            <th>Okres</th>
            <th>Aktywny?</th>
        </tr>
    </thead>
</table>

<table id="systemTable" class="display" style="width: 100%; margin: auto;">
    <thead>
        <tr>
            <th>Nazwa systemu</th>
            <th>Opis</th>
            <th>Opis technologii</th>
            <th></th>
        </tr>
    </thead>
</table>

<button id="createSystemButton" class="ui-button ui-widget ui-corner-all">Stwórz system</button>

<div id="createSystemDialog" title="Create system">
    <p id="createSystemTips"></p>
    <form id="createSystemForm">
        <label for="createSystemName" class="lab">Nazwa systemu:</label>
        <input maxlength="64" type="text" name="createSystemName" id="createSystemName"
               class="text ui-widget-content ui-corner-all">
        <label for="createDescription" class="lab">Opis:</label>
        <textarea maxlength="1024" id="createDescription" class="text ui-widget-content ui-corner-all"
                  style="height: 150px;"></textarea>
        <label for="createTechDescription" class="lab">Opis technologii:</label>
        <textarea maxlength="1024" id="createTechDescription" class="text ui-widget-content ui-corner-all"
                  style="height: 150px;"></textarea>
    </form>
</div>

<div id="editSystemDialog" title="Edit system">
    <p id="editSystemTips"></p>
    <form id="editSystemForm">
        <label for="editSystemName" class="lab">Nazwa systemu:</label>
        <input maxlength="64" type="text" name="editSystemName" id="editSystemName"
               class="text ui-widget-content ui-corner-all">
        <label for="editDescription" class="lab">Opis:</label>
        <textarea maxlength="1024" id="editDescription" class="text ui-widget-content ui-corner-all"
                  style="height: 150px;"></textarea>
        <label for="editTechDescription" class="lab">Opis technologii:</label>
        <textarea maxlength="1024" id="editTechDescription" class="text ui-widget-content ui-corner-all"
                  style="height: 150px;"></textarea>
    </form>
</div>

<div id="exportDialog" title="Exportuj plik do bazy">
    <p id="exportTips"></p>
    <form id="exportForm">
        <label for="exportInput" class="lab">Plik:</label>
        <input type="file" name="exportInput" id="exportInput" class="ui-widget ui-corner-all">
    </form>
</div>

<div id="exportStatisticsDialog" title="Statystki eksportu">
    <div>
        <p class="inline">Umowy dodane pomyślnie:</p>
        <p class="inline" id="successfulAdded"></p>
    </div>
    <div>
        <p class="inline">Umowy dostępne w bazie:</p>
        <p class="inline" id="alreadyAvailable"></p>
    </div>
    <div>
        <p class="inline">Umowy dla którch system nie istnieje:</p>
        <p class="inline" id="systemNotAvailable"></p>
    </div>
    <div>
        <p class="inline">Zły format wartości:</p>
        <p class="inline" id="wrongAmount"></p>
    </div>
    <div>
        <p class="inline">Zły foramt daty rozpoczęcia:</p>
        <p class="inline" id="wrongStartDate"></p>
    </div>
    <div>
        <p class="inline">Zły foramt daty zakończenia:</p>
        <p class="inline" id="wrongEndDate"></p>
    </div>
    <div>
        <p class="inline">Zły format numeru umowy:</p>
        <p class="inline" id="wrongOrderNumber"></p>
    </div>
    <div>
        <p class="inline">Zły format typu wartości:</p>
        <p class="inline" id="wrongAmountType"></p>
    </div>
    <div>
        <p class="inline">Zły format okresu wartości:</p>
        <p class="inline" id="wrongAmountPeriod"></p>
    </div>
    <div>
        <p class="inline">Zły format wrtości aktywności:</p>
        <p class="inline" id="wrongActive"></p>
    </div>
</div>

<div id="successDialog" title="Informacja">
    <p>
        <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span>
        Operacja zakończona pomyślnie
    </p>
</div>

<div id="about" title="O programie">
    <h1>Contract administrator</h1>
    <p>Autor: Mateusz Boral</p>
    <p>Wersja: 0.9.0</p>
    <p>Tu będą pozdrowienia, jak wymyśle coś fajnego</p>
</div>