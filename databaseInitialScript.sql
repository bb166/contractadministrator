create sequence systems_sequence increment by 1 minvalue 1 no maxvalue start with 1;
create sequence contracts_sequence increment by 1 minvalue 1 no maxvalue start with 1;

create table systems(system_id bigint default nextval('systems_sequence') primary key,
name varchar(64) not null unique,
description varchar(1024) not null,
technology_description varchar(1024) not null
);

create table amount_period(amount_period int primary key, name varchar(7));
insert into amount_period values (0,'Monthly'), (1, 'Yearly');
create table amount_type(amount_type int primary key, name varchar(5));
insert into amount_type values (0, 'Net'), (1, 'Gross');

create table contracts(contract_id bigint default nextval('contracts_sequence') primary key, 
contract_number varchar(32) unique not null,
start_date timestamp not null,
end_date timestamp not null,
amount numeric(19,2) not null,
active boolean not null default true,
amount_type int not null references amount_type(amount_type),
amount_period int not null references amount_period(amount_period),
system_id bigint not null references systems(system_id));

create unique index system_name_index on systems(name);
create unique index contract_number_index on contracts(contract_number);

insert into systems(name, description, technology_description) 
values 
('KUCYK','Przykładowy opis dla systemu KUCYK', 'Przykładowy opis technologii dla systemu KUCYK'),
('ŁÓDKA','Przykładowy opis dla systemu ŁÓDKA', 'Przykładowy opis technologii dla systemu ŁÓDKA'),
('KAPISZON','Przykładowy opis dla systemu KAPISZON', 'Przykładowy opis technologii dla systemu KAPISZON'),
('KOTEK','Przykładowy opis dla systemu KOTEK', 'Przykładowy opis technologii dla systemu KOTEK'),
('DEMON','Przykładowy opis dla systemu DEMON', 'Przykładowy opis technologii dla systemu DEMON'),
('ZÓŁWIK','Przykładowy opis dla systemu ZÓŁWIK', 'Przykładowy opis technologii dla systemu ZÓŁWIK'),
('KOJOTEK','Przykładowy opis dla systemu KOJOTEK', 'Przykładowy opis technologii dla systemu KOJOTEK');

insert into contracts(contract_number, start_date, end_date, amount, active, amount_type, amount_period, system_id) values 
('11/2011', '2011-11-11', '2012-11-11', 1111.11, true, 1, 1, 2),
('12/2011', '2011-12-11', '2012-12-11', 1121.11, false, 0, 0, 3);